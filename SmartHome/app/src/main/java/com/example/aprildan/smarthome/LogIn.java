package com.example.aprildan.smarthome;

import android.app.FragmentTransaction;
import android.nfc.Tag;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LogIn extends AppCompatActivity {

    EditText uname, password;
    Button login;
    TextView forgot, newacc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in);

        uname = (EditText)findViewById(R.id.etusername);
        password = (EditText)findViewById(R.id.etpassword);
        login = (Button)findViewById(R.id.btnlogin);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               uname.setError("Invalid username");
                password.setError("Invalid password");
            }
        });

        forgot = (TextView)findViewById(R.id.tv_forgot);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.setVisibility(View.INVISIBLE);
                uname.setVisibility(View.INVISIBLE);
                password.setVisibility(View.INVISIBLE);
                ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(
                        R.id.log_in,
                        forgotPasswordFragment,
                        forgotPasswordFragment.getTag()
                ).commit();
            }
        });

        newacc = (TextView)findViewById(R.id.tvnewacc);
        newacc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    login.setVisibility(View.INVISIBLE);
                    uname.setVisibility(View.INVISIBLE);
                    password.setVisibility(View.INVISIBLE);
                    CreateUserFragment createUserFragment= new CreateUserFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(
                            R.id.log_in,
                            createUserFragment,
                            createUserFragment.getTag()
                    ).commit();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
